# Express Discord Message System

익스프레스 웹서버에서 디스코드 채널로 메시지를 보내는 템플릿입니다.

## 설치 방법
> 1. node.js 설치
> 2. git 레포지토리 복제
> 3. 클론한 디렉터리에서 npm install 실행
> 4. npm start 실행