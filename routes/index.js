var express = require('express');
var router = express.Router();

let client

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { client: client });
});

router.post('/message', function(req, res, next) {
  client.channels.cache.find(channels => channels.id == req.body.channel).send(req.body.message)
  res.redirect('/')
})

exports.router = router;

exports.init = bot => {
  client = bot
}